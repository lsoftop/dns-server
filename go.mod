module gitlab.com/lsoftop/dns-server

go 1.13

require (
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/miekg/dns v1.1.17
	github.com/prometheus/client_golang v1.1.0
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/tools v0.0.0-20191018000036-341939e08647 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/ini.v1 v1.47.0
)
